<img src="https://code.freedombone.net/bashrc/smolrss/raw/master/logo.png?raw=true" width=200/>

# Smol RSS

Smol RSS is a minimal RSS reader. Minimal code means low maintenance, no database to maintain and small attack surface. Will run equally well on desktop or mobile.

You can create a file containing RSS feed URLs and their titles, then use the *create_feeds* script to turn that into *feeds.xml*.

``` bash
./create_feeds feeds.example.txt > feeds.xml
```

Install to a web server and you can then select a feed from the drop down menu and view it.

By default a dark theme is used, but if you prefer a light one:

``` bash
cp style.light.css style.css
```
<img src="https://code.freedombone.net/bashrc/smolrss/raw/master/img/screenshot_dark.jpg?raw=true" width=200/>
