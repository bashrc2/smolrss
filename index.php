<!DOCTYPE html>
<html>
<head>
    <meta charset='utf8' />
    <title>Smol RSS</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php
$xmlDoc = new DOMDocument();
$xmlDoc->load("feeds.xml");
$feed = $xmlDoc->getElementsByTagName("feed");

echo"<center><select name='feedslist' id='feedslist' onchange='window.location=\"?feed=\"+value'>";
echo"<option>Select RSS feed:</option>";
foreach($feed as $fe){
    $title = $fe->getElementsByTagName('title')->item(0)->nodeValue;
    $link = $fe->getElementsByTagName('link')->item(0)->nodeValue;

    echo "<option value='{$link}'>".$title."</option>";
}
echo "</select></center>";


if(isset($_GET['feed'])){
    $xmlDoc->load($_GET['feed']);

    $news = $xmlDoc->getElementsByTagName("item");

    foreach($news as $article) {
        $title = $article->getElementsByTagName("title")->item(0)->nodeValue;
        $link = $article->getElementsByTagName("link")->item(0)->nodeValue;
        $description = $article->getElementsByTagName("description")->item(0)->nodeValue;
        try {
            $enclosure = $article->getElementsByTagName("enclosure")->item(0);
        } catch (Exception $e) {
            $enclosure = '';
        }

        echo "<div id='box'>";

        echo "<h3>" . $title . "</h3><br />";
        echo $description . "<br />";
        echo "<hr />";

        if ($enclosure != '') {
            $enclosure = $article->getElementsByTagName("enclosure")->item(0)->getAttribute('url');
            echo "<table style='width:100%'><tr>";
            echo "<th><a href='{$enclosure}' target='_blank'><img src='media.png' alt='Play' width=100/></a></th>";
            echo "<th><a href='{$link}' target='_blank'><img src='read.png' alt='Read more' width=100/></a></th>";
            echo "</tr></table>";
        }
        else {
            echo "<a href='{$link}' target='_blank'><img src='read.png' alt='Read more' width=100/></a><br />";
        }

        echo "</div>";
    }

    $news = $xmlDoc->getElementsByTagName("entry");

    foreach($news as $article) {
        $title = $article->getElementsByTagName("title")->item(0)->nodeValue;
        $link = $article->getElementsByTagName("content")->item(0)->getAttribute('xml:base');
        if ($link == '') {
            $link = $article->getElementsByTagName("link")->item(0)->getAttribute('href');
        }
        $description = $article->getElementsByTagName("summary")->item(0)->nodeValue;
        if ($description == '') {
            $description = $article->getElementsByTagName("content")->item(0)->nodeValue;
        }
        try {
            $enclosure = $article->getElementsByTagName("enclosure")->item(0);
        } catch (Exception $e) {
            $enclosure = '';
        }

        echo "<div id='box'>";

        echo "<h3>" . $title . "</h3><br />";
        echo $description . "<br />";
        echo "<hr />";

        if ($enclosure != '') {
            $enclosure = $article->getElementsByTagName("enclosure")->item(0)->getAttribute('url');
            echo "<table style='width:100%'><tr>";
            echo "<th><a href='{$enclosure}' target='_blank'><img src='media.png' alt='Play' width=100/></a></th>";
            echo "<th><a href='{$link}' target='_blank'><img src='read.png' alt='Read more' width=100/></a></th>";
            echo "</tr></table>";
        }
        else {
            echo "<a href='{$link}' target='_blank'><img src='read.png' alt='Read more' width=100/></a><br />";
        }

        echo "</div>";
    }
}
?>
</body>
</html>
